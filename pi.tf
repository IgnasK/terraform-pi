module atan1over5 {
    source = "./atan"

    z = 1/5
}

module atan1over239 {
    source = "./atan"

    z = 1/239
}

locals {
    # Machin's formula: pi/4 = 4*atan(1/4)-atan(1/239)
    pi = 4*( 4*module.atan1over5.arctan - module.atan1over239.arctan )
}

output pi {
    value = local.pi
}