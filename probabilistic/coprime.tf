variable n {
  type = number
  default = 1000
}

resource "random_integer" "number" {
  count = 2 * var.n

  min = 1
  max = 200
}

locals {

  pairs = [for i in range(var.n): [random_integer.number[i*2].result, random_integer.number[i*2+1].result]]

  coprime = length([
    for p in local.pairs:
    1 if max([
      for c in range(1, min(p...)+1):
      c if p[0] % c == 0 && p[1] % c == 0
    ]...) == 1
  ])

  ratio = local.coprime / var.n
  pi = pow(6/local.ratio, 1/2)

}

output "pi" {
  value = local.pi
}