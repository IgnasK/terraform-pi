variable z {
  type = number
}

variable precision {
    default = 10
}

locals {
    arctan = sum([
        for i in range(var.precision):
        pow(-1, i) * pow(var.z, 2*i+1) / (2 * i + 1)
    ])
}

output arctan {
    value = local.arctan
}