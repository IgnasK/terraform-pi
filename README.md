# Computing π with Terraform

This workspace computes an approximation of π by only using standard terraform features - no local_exec, or external programs.

For a slower and less accurate, but arguably more fun method, see [probabilistic method](probabilistic/).

## Method

π is computed using faster-converging version of Gregory-Leibniz series:

```math
\frac{π}{4} = 4\arctan(\frac{1}{4})-\arctan(\frac{1}{239}) \\~\\
\arctan(z) = z - \frac{z^3}{3} + \frac{z^5}{5} - \frac{z^7}{7} + ...
```

## Usage

To compute π:

```bash
terraform plan
```

Output:

```text
❯ terraform plan

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:

Terraform will perform the following actions:

Plan: 0 to add, 0 to change, 0 to destroy.

Changes to Outputs:
  + pi = 3.141592653589791866433084436984816708936779977706403683656988300208698946277493219337417877922384820195426392562331024023619745712146821146456928615086322
```

The code creates no resources and uses no providers, and computes π at plan time, so you do not have to `terraform init` or `terraform apply` the repository!
