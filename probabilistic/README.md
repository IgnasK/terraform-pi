# Computing π with Terraform - Monte Carlo method

This method computes the approximate probability that 2 randomly selected integers are coprime, and uses this information to compute an approximation of π.

For much faster and accurate method, see infinite series method in [the root of the respository](../README.md).

## Method

This method uses the fact that probability that 2 randomly chosen integers being [coprime](https://en.wikipedia.org/wiki/Coprime_integers) is $`\frac{6}{π^2}`$.

The code uses many `random_integers` resources to generate random pairs of numbers, and then computes greatest common divisor of each pair.

Since terraform does not have a way to implement recursive functions, greatest common divisor is computed by trying all possible divisors and chosing the maximum, instead of more conventional Euclidean algorithm.

## Usage

To compute π:

```bash
terraform init
terraform apply -auto-approve
```

Output:

```text
❯ terraform apply -auto-approve

... many lines of creating random_integer resources

Apply complete! Resources: 2000 added, 0 changed, 0 destroyed.

Outputs:

pi = 3.1943828249996997
```
